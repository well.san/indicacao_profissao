package br.com.itau.Profissao.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

@Entity
public class Profissao {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)

	private int idProfissao;

	@NotBlank
	private String nomeProfissao;
	

	public Profissao() {
		super();
	}

	public Profissao(String nomeProfissao) {
		this.nomeProfissao = nomeProfissao;
	}

	public String getNomeProfissao() {
		return nomeProfissao;
	}

	public void setNomeProfissao(String nomeProfissao) {
		this.nomeProfissao = nomeProfissao;
	}

	public int getIdProfissao() {
		return idProfissao;
	}

	public void setIdProfissao(int idProfissao) {
		this.idProfissao = idProfissao;
	}

}
