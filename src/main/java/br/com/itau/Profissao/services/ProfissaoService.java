package br.com.itau.Profissao.services;



import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.Profissao.models.Profissao;
import br.com.itau.Profissao.repository.ProfissaoRepository;


@Service
public class ProfissaoService {
	
	@Autowired
	ProfissaoRepository profissaoRepositorie;
	
	
	public void inserirListaProfissoes(List<Profissao> profissoes) {
		profissaoRepositorie.saveAll(profissoes);
	}
			
	
	public Iterable<Profissao> listarProfissao(){
		return profissaoRepositorie.findAll();
	}
	
	
	public boolean inserir(Profissao profissao) {
		
		Optional<Profissao> optionalProfissao = profissaoRepositorie.findByNomeProfissao(profissao.getNomeProfissao());
		if (optionalProfissao.isPresent()) {
			return false;	
		}			
		profissaoRepositorie.save(profissao);	
		return true;
			
	}
	
	public Profissao listarProfissaoPeloNome(int id) {
		
		return 	profissaoRepositorie.getByidProfissao(id);	
		
	}
	
	
}
