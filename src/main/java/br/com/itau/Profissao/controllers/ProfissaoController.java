package br.com.itau.Profissao.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.Profissao.models.Profissao;
import br.com.itau.Profissao.services.ProfissaoService;


@RestController
@RequestMapping("/insereprofissao")
@CrossOrigin
public class ProfissaoController {
	
	@Autowired
	ProfissaoService profissaoService;
	
	@PostMapping
	public boolean InserirProf(@RequestBody Profissao profissao) {
		return profissaoService.inserir(profissao);
	}
	
	
	@PostMapping("/lote")
	public ResponseEntity inserirProfissaoEmLote(@RequestBody List<Profissao> profissoes) {
		profissaoService.inserirListaProfissoes(profissoes);
		return ResponseEntity.status(201).build();
	}
	
	
	@GetMapping
	public Iterable<Profissao> listarProfissoes(){
		return profissaoService.listarProfissao();
	}

	@GetMapping("/{id}")
	public Profissao listarProfissaoPorId(@PathVariable int id) {
		
		  return profissaoService.listarProfissaoPeloNome(id);	   
		}
	
}
